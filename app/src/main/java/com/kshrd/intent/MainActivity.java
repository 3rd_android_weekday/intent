package com.kshrd.intent;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_CODE = 1;
    Button btnLoginScreen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ArrayList<Person> personList = new ArrayList<>();
        personList.add(new Person(100, "Dara"));
        personList.add(new Person(101, "Sok"));

        btnLoginScreen = (Button) findViewById(R.id.btnLoginScreen);
        btnLoginScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                intent.putExtra("name", "John");
                intent.putExtra("age", 18);
                intent.putExtra("person", new Person(1, "Mary"));
                intent.putParcelableArrayListExtra("personList", personList);
                startActivity(intent);
            }
        });

        findViewById(R.id.btnWebPage).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse("https://www.facebook.com"));

                startActivity(i);

                // Create intent to show chooser
//                Intent chooser = Intent.createChooser(i, "Choosing");
//                if (chooser.resolveActivity(getPackageManager()) != null ) {
//                    startActivity(chooser);
//                }

            }
        });

        findViewById(R.id.btnStartForResult).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ForResultActivity.class);
                intent.putParcelableArrayListExtra("PERSON_LIST", personList);
                startActivityForResult(intent, REQUEST_CODE);
            }
        });

        findViewById(R.id.btnSecondApp).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent("com.kshrd.intent.intent.action.TEST");
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE){
            if (resultCode == RESULT_OK){
                List<Person> list = data.getParcelableArrayListExtra("PERSON_LIST");
                for (Person p : list){
                    Log.e("ooooo", p.getName());
                }
            }
        }
    }
}
