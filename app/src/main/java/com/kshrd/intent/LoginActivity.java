package com.kshrd.intent;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import java.util.List;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        TextView tvDescription = (TextView) findViewById(R.id.tvDescription);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String name = bundle.getString("name");
            int age = bundle.getInt("age");
            tvDescription.setText("Name -> " + name + "\n" + "Age -> " + age);

            Person p = bundle.getParcelable("person");
            List<Person> list = bundle.getParcelableArrayList("personList");

            Log.e("ooooo", p.getName());
            for (Person person : list){
                Log.e("ooooo", person.getName());
            }
        }
    }
}
