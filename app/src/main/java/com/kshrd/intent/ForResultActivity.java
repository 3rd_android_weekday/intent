package com.kshrd.intent;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ForResultActivity extends AppCompatActivity {

    ArrayList<Person> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_for_result);

        Bundle bundle = getIntent().getExtras();
        list = bundle.getParcelableArrayList("PERSON_LIST");
        list.get(0).setName("New Name");

        findViewById(R.id.btnSetResult).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ForResultActivity.this, "", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void finish() {
        Intent i = new Intent();
        i.putParcelableArrayListExtra("PERSON_LIST", list);
        setResult(RESULT_OK, i);
        super.finish();
    }
}
